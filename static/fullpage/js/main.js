$(function() {
    $("#ido").fullpage({
        anchors:['section1', 'section2', 'section3','section4'],
        afterLoad:function(anchorLink, index) {
            if(index ==1) {
                $("#animate1").animate({
                    top:"50px",
                });
            }
            if(index == 2) {
                $("#title").animate({
                    marginTop:"0px" ,
                    opacity:"1"

                },300,function(){//callback
                    $("#features").animate({ 
                        marginTop:"50px", opacity:1
                    });
                    $(".feature-more").animate({ 
                        opacity:1
                    });
                });
            }
        },
        onLeave:function(index, nextIndex,direction) {
            if(index ==1) {
                $("#animate1").animate({
                    top:"0px",
                });
            }
            if(index == 2) {
                $("#title").animate({ 
                    marginTop:"40px",
                    opacity:"0",
                },800);
                $("#features").animate({ 
                    marginTop:"150px", 
                    opacity:"0",
                });
                $(".feature-more").animate({ 
                    opacity:"0",
                });
            }
        }
    });
});
