<?php
namespace Home\Model;

use Think\Model;

class CartModel extends Model {
    protected $tableName = 'cart';
    protected $fields = array (
        'id',//db generated
        'uid',//由controller提供，_validate验证必须为数字
        'product_id',//由表单提供， _validate验证必须为数字
        'quantity',//由表单提供， _validate验证必须为数字
        'add_time',//auto
        'add_ip',//auto
        'status'//auto 
    );
    protected $_auto = array (
        array (
            'add_ip',
            'get_client_ip',
            1,
            'function'
        ),
        array (
            'add_time',
            'gettime',
            1,//insert time
            'callback'
        ),
        array (
            'status',
            '1',
        )
    );
    protected $_validate = array (
        array (
            'uid',
            'number',//letters or numbers
            'uid must be a number'
        ),
        array (
            'product_id',
            'number',//letters or numbers
            'product_id must be a number'
        ),
        array (
            'quantity',
            'number',//letters or numbers
            'quantity must be a number'
        ),
    );
    protected $_map = array (
        'userpassword' => 'password',
    );
    protected $pk = 'id';

    function gettime() {
        return date('Y-m-d H:i:s',time());
    }

}
