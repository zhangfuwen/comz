<?php
namespace Home\Model;

use Think\Model;

class TestModel extends Model {
    protected $tableName = 'test';
    protected $fields = array (
        'id',
        'name',
        'age',
    );
    protected $_validate = array (
        array (
            'name',
            'require',
            'Name Empty OR Replicate',
            self::MUST_VALIDATE,
            'unique',
            self::MODEL_BOTH 
        ),
        array (
            'age',
            'number',
            'Age must be a number',
        ) 
    );
    protected $_map = array (
        'username' => 'name',
        'userage' => 'age',
    );
    protected $pk = 'id';
}
