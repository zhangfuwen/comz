<?php
namespace Home\Model;

use Think\Model;

function gensalt($in) {
    return String::randString ( 10 );
}

function gettime() {
    return date('Y-m-d H:i:s',time());
}
class UserModel extends Model {
    protected $tableName = 'user';
    protected $fields = array (
        'id',
        'username',
        'password',
        'repassword',
        'password_salt',
        'reg_time',
        'reg_ip',
        'last_login_time',
        'last_login_ip',
        'last_change_pwd_time',
        'status' 
    );
    public function isCorrectPassword($password) {
        return $this->addsalt($password) == $this->addsalt($fields['password']);
    }
    protected $_auto = array (
        array (
            'reg_ip',
            'get_client_ip',
            1,
            'function'
        ),
        array (
            'last_login_ip',
            'get_client_ip',
            3,
            'function'
        ),
        array (
            'username',
            '',
            3,
            'ignore'
        ),
        array (
            'password_salt',
            'gensalt',
            1,
            'callback'
        ),
        array (
            'password',
            'addsalt',
            3,
            'callback'
        ),
        /* don't store it into database
        array (
            'repassword',
            'addsalt',
            3,
            'callback'
        ),*/
        array (
            'repassword',
            '',
            3,
            'ignore'
        ),
        array (
            'repassword',
            'addsalt',
            3,
            'callback'
        ),
        array (
            'reg_time',
            'gettime',
            1,//insert time
            'callback'
        ),
        //reg_ip varchar(15),
        array (
            'last_login_time',
            'gettime',
            3,
            'callback'
        ),
//        last_login_ip varchar(15),
        array (
            'last_change_pwd_time',
            'gettime',
            1,//insert time
            'callback'
        ),
        array (
            'status',
            '1',
        )
    );
    protected $_validate = array (
        array (
            'username',
            '/^[A-Za-z]+[0-9]*$/',//letters or numbers
            'Username must contain only letters or numbers'
        ),
        array (
            'username',
            'require',
            'Username Empty OR Replicate',
            self::MUST_VALIDATE,
            'unique',//needs to access database
            self::MODEL_BOTH 
        ),
        array (
            'password',
            '8,12',
            'password of invalid length',
            self::MUST_VALIDATE,
            'length',
            self::MODEL_BOTH 
        ),
        array (
            'repassword',
            'password',
            'passwords you entered are different',
            self::MUST_VALIDATE,
//            'equal', wrong, this tests if repassword is equal to 'password', not $password
            'confirm',
            self::MODEL_BOTH 
        ),
    );
    protected $_map = array (
        'userpassword' => 'password',
    );
    protected $pk = 'id';

    protected function addsalt($password) {
        return md5( $fields['password_salt']+$password);
    }
    function genRandomString() {
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';

        for ($p = 0; $p < $length; $p++) {
                $string .= $characters[mt_rand(0, strlen($characters))];
            }

        return $string;
    }

    function gensalt($in) {
        return $this->genRandomString();
    }
    function gettime() {
        return date('Y-m-d H:i:s',time());
    }

}
