<?php

namespace Home\Controller;

class CartController extends BaseController
{
    public function init()
    {
        $Model = new \Think\Model();
        $Model->execute("create  table if not exists  tpx_cart (
            id integer primary key auto_increment,
        uid integer  ,
        product_id integer ,
        quantity integer ,
        add_time datetime,
        add_ip varchar(15),
        status integer 
        )");
        $this->get();
    }
    public function index()
    {
        redirect(U("User/profile"),2,"进入您的页面");
    }
    public function add_to_cart() {
        $uid= session('uid');
        if(empty($uid)) {
            redirect(U("User/login"),2,"您还没有登录");
        }
        $Cart =D('Cart');
        $data = array (
            'uid' => $uid,
            'product_id' => I("post.product_id"),
            'quantity' => I("post.quantity"),
        );

        if ( !$Cart->create($data)) {
            //validation not passed
            exit($Cart->getError());
        }
        $Cart->add();
        redirect(U("User/profile"),2,"正在进入购物车");

    }
    public function get_cart_for_this_user() {
        $uid= session('uid');
        if(empty($uid)) {
            return false;
        }
        $Cart = M();
        $data = $Cart->query('SELECT * FROM tpx_cart a, tpx_cms_product b where uid='.$uid.' and a.product_id = b.id');
        return $data;

    }

}
