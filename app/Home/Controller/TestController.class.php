<?php

namespace Home\Controller;

class TestController extends BaseController
{
    public function _empty($new_id = 0)
    {
        $pro_id = intval($new_id);
        $d = D('CmsNews')->find($new_id);
        if (empty ($d)) {
            $this->redirect('/');
        }

        $d ['content'] = parse_res_url($d ['content'], __ROOT__ . '/', __ROOT__ . '/');

        $this->data_news = $d;

        $this->assign('page_title', $d ['title'] . ' - ' . tpx_config_get('home_title'));
        $this->assign('page_keywords', $d ['keywords']);
        $this->assign('page_description', $d['description']);
        $this->display('view');
    }

    public function index()
    {
        $this->page();
    }

	public function handlepost() 
	{
		$m =D('Test');
        if ( !$m->create()) {
            //validation not passed
            exit($m->getError());
        }
        /*
		$data['name']=I('post.username');//$this->_post('username');
		$data['age'] =I('post.userage'); //$this->_post('userage');
         */
		if( empty($m->name) || empty($m->age)) {
		    redirect(U("Test/mylist"),2,"no data or invalid data");
		}
//		$m->create($data);
		$m->add();
		redirect(U("Test/mylist"),2,"跳转中");

	}
	public function mylist() 
	{
		$m =M('Test');
		$data=$m->select();
		$this->assign('data',$data);
		$this->assign('url', U("Test/mylist"));
		$this->display('mylist');

	}

    public function page($p = 1)
    {
        $p = intval($p);

        $m = M('CmsNews');

        $page = new \Think\Page ($m->count(), 10);
        $page->setConfig('prev', '上页');
        $page->setConfig('next', '下页');
        $this->data_page = $page->show('page');
		$this->assign('action',U("Test/handlepost"));

        $this->data_news = $m->order('id desc')->field('id,title,posttime,description,cover')->page($p, 10)->select();

        $this->assign('page_title', '新闻动态 - ' . tpx_config_get('home_title'));
        $this->assign('page_keywords', '新闻动态 - ' . tpx_config_get('home_keywords'));
        $this->assign('page_description', '新闻动态 - ' . tpx_config_get('home_description'));
        $this->display('page');
    }
}
